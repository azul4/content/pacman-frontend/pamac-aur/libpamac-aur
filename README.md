# libpamac-aur

Pamac package manager library based on libalpm

https://gitlab.manjaro.org/applications/libpamac

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/pacman-frontend/pamac-aur/libpamac-aur.git
```

